import argparse
import subprocess
import os
import time
import re
import bcrypt

from pathlib import Path
from dotenv import dotenv_values

parser = argparse.ArgumentParser(description='Deploys eregistrations application to the server.'
                                             ' Optional flags starting with "step-" can be used only one at a time.')
env_path = Path('../compose/eregistrations/') / '.env'
config = dotenv_values(dotenv_path=env_path)

auth_service = ''
user_id = os.getuid()
postgres_user_id = int(subprocess.run('id -u postgres', shell=True, stdout=subprocess.PIPE).stdout)

supported_steps = {
    'all': 'run all steps',
    'auth': 'select authentication service',
    'translation': 'translation service cleanup',
    'pwd': '(re)generate passwords',
    'postgres': 'create postgres users and databases',
    'mongo': 'create mongo users and databases',
    'haproxy': 'configure haproxy',
    'network': 'create docker network',
    'deploy': 'deploy docker images',
    'prepare_auth_service': 'prepare auth service'
}

requiredEnvVariables = {
    'deploy_docker_images': [
        'ACTIVEMQ_PASSWORD', 'ACTIVEMQ_USER', 'CAMUNDA_PASSWORD', 'FORMIO_EMAIL',
        'FORMIO_PASSWORD', 'RESTHEART_PASSWORD', 'GRAYLOG_ROOT_USERNAME', 'GRAYLOG_ROOT_PASSWORD',
        'MAIL_HOST', 'MAIL_PORT', 'MAIL_USERNAME', 'MAIL_PASSWORD', 'MAIL_REPLY_TO', 'MAIL_FROM',
        'SYSTEM_CODE', 'DEFAULT_LANGUAGE', 'YOUR_DOMAIN_NAME', 'TIME_ZONE', 'PATH_TO_COMPOSE_CONF',
        'DOCKER_USERNAME', 'DOCKER_PASSWORD', 'SERVICE_HOST', 'TRANSLATIONS_SYSTEM_INTERNAL', 'PATH_TO_GLOBAL_COMPOSE_CONF'
    ],
    'configure_haproxy': [
        'SYSTEM_CODE', 'YOUR_DOMAIN_NAME', 'PATH_TO_HAPROXY_CONF', 'PATH_TO_SSL_CERTIFICATE'
    ],
    'create_docker_network': ['SERVICE_HOST']
}

auto_generatable_pwds = (
    'BPA_POSTGRES_DB_PASSWORD', 'CAMUNDA_POSTGRES_DB_PASSWORD', 'CAS_POSTGRES_DB_PASSWORD', 'CASHIER_POSTGRES_DB_PASSWORD',
    'DS_POSTGRES_DB_PASSWORD', 'GDB_POSTGRES_DB_PASSWORD', 'KEYCLOAK_POSTGRES_DB_PASSWORD', 'PARTC_POSTGRES_DB_PASSWORD',
    'STATISTICS_POSTGRES_DB_PASSWORD', 'TRANSLATION_SERVICE_POSTGRES_DB_PASSWORD', 'FORMIO_MONGO_DB_PASSWORD', 'RESTHEART_MONGO_DB_PASSWORD',
    'CAMUNDA_PASSWORD', 'RESTHEART_PASSWORD', 'FORMIO_PASSWORD', 'BPA_FE_OAUTH_CLIENT_SECRET', 'CAS_FE_OAUTH_CLIENT_SECRET',
    'DS_OAUTH_CLIENT_SECRET', 'GDB_OAUTH_CLIENT_SECRET', 'PARTC_FE_OAUTH_CLIENT_SECRET', 'STATISTICS_FE_OAUTH_CLIENT_SECRET',
    'KEYCLOAK_ADMIN_USER_PASSWORD', 'CAMUNDA_OAUTH_CLIENT_SECRET', 'BPA_BE_OAUTH_CLIENT_SECRET', 'STATISTICS_BE_OAUTH_CLIENT_SECRET',
    'GRAYLOG_MONGO_DB_PASSWORD', 'MINIO_ROOT_PASSWORD', 'OPENSEARCH_ADMIN_PASSWORD'
)


def print_title(fn_name):
    print()
    print()
    print('------------------------------')
    print(' %s ' % fn_name)
    print('------------------------------')


def validate_required_env_variables(fn_name):
    valid = True
    for key in requiredEnvVariables[fn_name]:
        variable = config.get(key)
        if not variable:
            print('Variable %s is not defined in .env' % key)
            valid = False
    return valid


def create_postgres_database(prefix):
    db_name = config.get(prefix + '_POSTGRES_DB_NAME')
    db_user = config.get(prefix + '_POSTGRES_DB_USER')
    db_password = config.get(prefix + '_POSTGRES_DB_PASSWORD')
    conf_invalid = False
    login_authorized = False

    if not db_name:
        print(prefix + "_POSTGRES_DB_NAME variable is not defined in .env")
        conf_invalid = True
    if not db_user:
        print(prefix + "_POSTGRES_DB_USER variable is not defined in .env")
        conf_invalid = True
    if not db_password or db_password.strip() == '#changeme':
        print(prefix + "_POSTGRES_DB_PASSWORD variable is not defined in .env")
        conf_invalid = True
    if conf_invalid:
        return

    os.setreuid(postgres_user_id, user_id)
    command = subprocess.run("psql -d postgres -q -c 'CREATE USER %s WITH ENCRYPTED PASSWORD $$%s$$;'"
                             % (db_user, db_password), shell=True, stderr=subprocess.PIPE)
    if command.returncode == 1:
        if 'already exists' in command.stderr.decode('utf-8'):
            print('%s database user already exists' % db_user)
            print('testing users login credentials')
            command = subprocess.run("PGPASSWORD=%s psql -U %s -d %s -c 'SELECT 1;'" % (db_password, db_user, db_name)
                                     , shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            if command.returncode == 0:
                print('%s database user login credentials are accurate' % db_user)
                login_authorized = True
            else:
                print('%s database user login credentials are inaccurate' % db_user)
                print('updating login credentials for %s database user' % db_user)
                command = subprocess.run("psql -d postgres -q -c 'ALTER USER %s WITH ENCRYPTED PASSWORD $$%s$$;'"
                                         % (db_user, db_password), shell=True)
                if command.returncode == 1:
                    print('Problem occurred while altering postgre user %s password, continuing' % db_user)
                    os.setreuid(user_id, user_id)
                    return
        else:
            print('Problem occurred while creating postgre user %s, continuing' % db_user)
            os.setreuid(user_id, user_id)
            return

    command = subprocess.run("psql -d postgres -q -c 'CREATE DATABASE %s WITH OWNER %s;'"
                             % (db_name, db_user), shell=True, stderr=subprocess.PIPE)
    if command.returncode == 1:
        if 'already exists' in command.stderr.decode('utf-8'):
            print('%s database already exists, overriding the owner to %s' % (db_name, db_user))
            command = subprocess.run("psql -d postgres -q -c 'ALTER DATABASE %s OWNER TO %s;'"
                                     % (db_name, db_user), shell=True, stderr=subprocess.PIPE)
            if command.returncode == 1:
                os.setreuid(user_id, user_id)
                print('Problem occurred while creating postgre db %s, continuing' % db_name)
                return
            else:
                print('%s database owner overwritten to %s' % (db_name, db_user))
        else:
            os.setreuid(user_id, user_id)
            print('Problem occurred while creating postgre db %s, continuing' % db_name)
            return

    if db_name == 'display_system':
        command = subprocess.run("psql -d %s -q -c 'CREATE EXTENSION IF NOT EXISTS pg_trgm;'"
                                 % (db_name), shell=True, stderr=subprocess.PIPE)

    if db_name == 'bpa':
        command = subprocess.run(f"""psql -d {db_name} -q -c 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp";'""", shell=True, stderr=subprocess.PIPE)

    if db_name == 'gdb':
        command = subprocess.run("psql -d %s -q -c 'CREATE EXTENSION IF NOT EXISTS pg_trgm;'"
                                 % (db_name), shell=True, stderr=subprocess.PIPE)
        command = subprocess.run(f"""psql -d {db_name} -q -c "UPDATE pg_opclass SET opcdefault = true WHERE opcname = 'gin_trgm_ops';" """
                                 , shell=True, stderr=subprocess.PIPE)
        command = subprocess.run("psql -d %s -q -c 'CREATE EXTENSION IF NOT EXISTS pgcrypto;'"
                                 % (db_name), shell=True, stderr=subprocess.PIPE)
        command = subprocess.run("psql -d %s -q -c 'CREATE EXTENSION IF NOT EXISTS fuzzystrmatch;'"
                                 % (db_name), shell=True, stderr=subprocess.PIPE)
                                 
    if db_name == 'camunda':
        command = subprocess.run(f"psql -d {db_name} -q -c 'GRANT CONNECT ON DATABASE camunda TO statistics;'", shell=True, stderr=subprocess.PIPE)
        command = subprocess.run(f"psql -d {db_name} -q -c 'GRANT USAGE ON SCHEMA public TO statistics;'", shell=True, stderr=subprocess.PIPE)
        command = subprocess.run(f"psql -d {db_name} -q -c 'GRANT SELECT ON ALL TABLES IN SCHEMA public TO statistics;'", shell=True, stderr=subprocess.PIPE)
        command = subprocess.run(f"psql -d {db_name} -q -c 'ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO statistics;'", shell=True, stderr=subprocess.PIPE)
    
    if db_name == 'statistics':
        command = subprocess.run(f"psql -d {db_name} -q -c 'CREATE EXTENSION IF NOT EXISTS dblink;'", shell=True, stderr=subprocess.PIPE)

    if not login_authorized:
        command = subprocess.run("PGPASSWORD=%s psql -U %s -d %s -c 'SELECT 1;'" % (db_password, db_user, db_name)
                                 , shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if command.returncode == 0:
            print('%s database user login credentials are accurate' % db_user)
        else:
            print('%s database user login credentials are not working' % db_user)
            print('Please simplify %s database users password' % db_user)
            os.setreuid(user_id, user_id)
            return
    print('Postgres %s database preparation finished with %s user' % (db_name, db_user))
    os.setreuid(user_id, user_id)

def user_exists_and_update_password(db_name, db_user, db_password):
    check_user_command = (
        f'mongosh --eval "db.getSiblingDB(\'{db_name}\').getUser(\'{db_user}\') != null"'
    )
    result = subprocess.run(check_user_command, shell=True, capture_output=True, text=True)

    if 'true' in result.stdout:
        print(f"User '{db_user}' exists in database '{db_name}', updating password.")
        update_user_command = (
            f'mongosh --eval "db.getSiblingDB(\'{db_name}\').updateUser(\'{db_user}\', {{ pwd: \'{db_password}\' }})"'
        )
        subprocess.run(update_user_command, shell=True)
        return True
    return False

def create_mongo_database(prefix):
    db_name = config.get(prefix + '_MONGO_DB_NAME')
    db_user = config.get(prefix + '_MONGO_DB_USER')
    db_password = config.get(prefix + '_MONGO_DB_PASSWORD')
    conf_invalid = False

    if not db_name:
        print(prefix + "_MONGO_DB_NAME variable is not defined in .env")
        conf_invalid = True
    if not db_user:
        print(prefix + "_MONGO_DB_USER variable is not defined in .env")
        conf_invalid = True
    if not db_password or db_password.strip() == '#changeme':
        print(prefix + "_MONGO_DB_PASSWORD variable is not defined in .env")
        conf_invalid = True
    if conf_invalid:
        return

    if user_exists_and_update_password(db_name, db_user, db_password):
        return

    # restheart exception. The user needs more rights than other users.
    if prefix == 'RESTHEART':
        subprocess.run('mongosh --eval "db.getSiblingDB(\'%s\').createUser({ user: \'%s\', pwd: \'%s\','
                       'roles: [\'root\', { role: \'readWriteAnyDatabase\', db: \'%s\'}]});"'
                       % (db_name, db_user, db_password, db_name), shell=True)
    else:
        subprocess.run('mongosh --eval "db.getSiblingDB(\'%s\').createUser({ user: \'%s\', pwd: \'%s\','
                       'roles: [{ role: \'readWrite\', db: \'%s\'}]});"'
                       % (db_name, db_user, db_password, db_name), shell=True)


def create_postgres_databases():
    print_title('create_postgres_databases')
    for key, value in config.items():
        if key.upper().endswith('_POSTGRES_DB_NAME'):
            create_postgres_database(key.upper()[:-17])


def create_mongo_databases():
    print_title('create_mongo_databases')
    # disable security for mongo
    subprocess.run('sed -i -e "s|authorization: enabled|authorization: disabled|g" /etc/mongod.conf', shell=True)
    subprocess.run('systemctl restart mongod', shell=True)
    # it takes couple of seconds for  the mongo to restart
    time.sleep(5)
    for key, value in config.items():
        if key.upper().endswith('_MONGO_DB_NAME'):
            create_mongo_database(key.upper()[:-14])

    # enable security for mongo
    subprocess.run('sed -i -e "s|authorization: disabled|authorization: enabled|g" /etc/mongod.conf', shell=True)
    subprocess.run('systemctl restart mongod', shell=True)
    # it takes couple of seconds for  the mongo to restart
    time.sleep(5)


def configure_haproxy():
    print_title('configure_haproxy')
    print('starting to configure haproxy config process')
    print('validating .env variables related to haproxy')
    if not validate_required_env_variables('configure_haproxy'):
        print('validating .env variables related to haproxy failed')
        return

    with open(config.get('PATH_TO_HAPROXY_CONF') + 'haproxy.cfg', "r") as f:
        data = f.read()

    chop = re.compile(' #changeme', re.DOTALL)
    data = chop.sub('', data)

    chop = re.compile('SYSTEM_CODE', re.DOTALL)
    data = chop.sub(config.get('SYSTEM_CODE'), data)

    chop = re.compile('YOUR_DOMAIN_NAME', re.DOTALL)
    data = chop.sub(config.get('YOUR_DOMAIN_NAME'), data)

    chop = re.compile('PATH_TO_SSL_CERTIFICATE', re.DOTALL)
    data = chop.sub(config.get('PATH_TO_SSL_CERTIFICATE'), data)

    with open(config.get('PATH_TO_HAPROXY_CONF') + 'haproxy.cfg', "w") as f:
        f.write(data)

    print('haproxy configuration changed')
    print('starting to create haproxy symlinks')
    # change the name of default haproxy config file
    subprocess.run('mv /etc/haproxy/haproxy.cfg /etc/haproxy/haproxy.cfg-backup', shell=True)
    subprocess.run('rm -f /etc/haproxy/cas-bot-token-whitelist.lst', shell=True)
    subprocess.run('rm -f /etc/haproxy/options.http', shell=True)

    subprocess.run('ln -s %shaproxy.cfg /etc/haproxy/haproxy.cfg' % config.get('PATH_TO_HAPROXY_CONF'), shell=True)
    subprocess.run('ln -s %scas-bot-token-whitelist.lst /etc/haproxy/cas-bot-token-whitelist.lst'
                   % config.get('PATH_TO_HAPROXY_CONF'), shell=True)
    subprocess.run('ln -s %soptions.http /etc/haproxy/options.http' % config.get('PATH_TO_HAPROXY_CONF'), shell=True)

    print('Haproxy symlinks created')
    print('Testing haproxy config')
    result = subprocess.run('/usr/sbin/haproxy -c -V -f /etc/haproxy/haproxy.cfg', shell=True,
                            stdout=subprocess.PIPE).stdout
    if 'Configuration file is valid' not in result.decode('utf-8'):
        print('haproxy configuration has a problem please review')
        return
    print('Starting haproxy service')
    subprocess.run('systemctl start haproxy', shell=True)


def create_docker_network():
    print_title('create_docker_network')
    print('starting to create bridge docker network process')
    print('validating .env variables related to docker network')
    if not validate_required_env_variables('create_docker_network'):
        print('validating .env variables related to docker network failed')
        return

    result = subprocess.run('docker network ls | grep deploy_default', shell=True, stdout=subprocess.PIPE).stdout
    # if result exists then network already created by sysadmin
    if not result:
        gateway = config.get('SERVICE_HOST')
        subnet = gateway[:-1] + '0'
        subprocess.run('docker network create -d bridge --subnet %s/16 --gateway %s deploy_default'
                       % (subnet, gateway), shell=True)
    else:
        print('bridge network already exists')


def deploy_from_compose(compose_dir):
    global auth_service
    if not auth_service:
        auth_selection(True)

    os.chdir(os.path.join('%s' % compose_dir))
    print('pulling first set of docker images')
    subprocess.run('docker-compose pull activemq opensearch-node1 minio', shell=True)
    print('images pull completed')
    print('deploying first set of docker images')
    subprocess.run('docker-compose up -d activemq opensearch-node1 minio', shell=True)

    print('pulling second set of docker images')
    subprocess.run('docker-compose pull graylog restheart formio', shell=True)
    print('images pull completed')
    print('deploying second set of docker images')
    subprocess.run('docker-compose up -d graylog restheart formio', shell=True)

    print('pulling third set of docker images')
    if auth_service == "KEYCLOAK":
        subprocess.run('docker-compose pull keycloak', shell=True)
        print('images pull completed')
        print('deploying third set of docker images')
        subprocess.run('docker-compose up -d keycloak', shell=True)
    else:
        print('pulling third set of docker images')
        subprocess.run('docker-compose pull cas-backend cas-frontend', shell=True)
        print('images pull completed')
        print('deploying trird set of docker images')
        subprocess.run('docker-compose up -d cas-backend cas-frontend', shell=True)

    print('pulling last set of docker images')
    subprocess.run('docker-compose pull', shell=True)
    print('images pull completed')
    print('deploying last set of docker images')
    subprocess.run('docker-compose up -d', shell=True)


def deploy_docker_images():
    print_title('deploy_docker_images')
    print('starting to deploy docker images process')
    print('validating .env variables related to deploying docker images')
    if not validate_required_env_variables('deploy_docker_images'):
        print('validating .env variables related to deploying docker images failed')
        return
    print('logging into docker')
    subprocess.run('docker login -u %s --password-stdin'
                   % config.get('DOCKER_USERNAME'), shell=True, input=config.get('DOCKER_PASSWORD'), encoding='utf-8')
    deploy_from_compose(config.get('PATH_TO_COMPOSE_CONF'))
    if config.get('TRANSLATIONS_SYSTEM_INTERNAL') != 'False':
        deploy_from_compose(config.get('PATH_TO_GLOBAL_COMPOSE_CONF'))


def dont_return_true_before_cas_backend_is_running():
    print('testing for cas backend to be running')
    command = subprocess.run("curl http://localhost:8282/cback/v1.0/actuator/health"
                             , shell=True, stdout=subprocess.PIPE)
    if command.returncode == 0 and command.stdout:
        if '{"status":"UP"}' == command.stdout.decode('utf-8'):
            print('cas backend is up')
            return True
    print('cas backend is not up yet.')
    print('waiting for 5 seconds and trying again...')
    time.sleep(5)
    return dont_return_true_before_cas_backend_is_running()


def create_micro_service(prefix):
    client_id = config.get(prefix + '_OAUTH_CLIENT_ID')
    secret = config.get(prefix + '_OAUTH_CLIENT_SECRET')
    redirect_url = config.get(prefix + '_OAUTH_CLIENT_REDIRECT_URL')
    db_name = config.get('CAS_POSTGRES_DB_NAME')
    conf_invalid = False

    if not db_name:
        print("CAS_POSTGRES_DB_NAME variable is not defined in .env")
        conf_invalid = True
    if not client_id:
        print(prefix + "_OAUTH_CLIENT_ID variable is not defined in .env")
        conf_invalid = True
    if not redirect_url:
        print(prefix + "_OAUTH_CLIENT_REDIRECT_URL variable is not defined in .env")
        conf_invalid = True
    if not secret or secret.strip() == '#changeme':
        print(prefix + "_OAUTH_CLIENT_SECRET variable is not defined in .env")
        conf_invalid = True
    if conf_invalid:
        return

    secret = bcrypt.hashpw(secret.encode(), bcrypt.gensalt(10))
    os.setreuid(postgres_user_id, user_id)
    command = subprocess.run("psql -d %s -q -c 'INSERT INTO cas.micro_service (client_id, secret, redirect_uri, grants)"
                             " VALUES ($$%s$$, $$%s$$, $$%s$$, $$authorization_code,refresh_token$$);'"
                             % (db_name, client_id, secret.decode(), redirect_url), shell=True, stderr=subprocess.PIPE)
    if command.returncode == 1:
        print('problem occurred while creating oauth2_client for %s' % prefix)
        print(command.stderr.decode('utf-8'))
    else:
        print('oauth2 client created for %s' % prefix)
    os.setreuid(user_id, user_id)
    print('deploy finished')


def prepare_keycloak_realm():
    with open(config.get('PATH_TO_SCRIPT_DIR') + 'keycloak-realm.json', "r") as f:
        data = f.read()

    chop = re.compile('REALM_PLACEHOLDER', re.DOTALL)
    data = chop.sub(config.get('SYSTEM_CODE'), data)

    chop = re.compile('REALM_LOWERCASE_PLACEHOLDER', re.DOTALL)
    data = chop.sub(config.get('SYSTEM_CODE').lower(), data)

    chop = re.compile('YOUR_DOMAIN_NAME_PLACEHOLDER', re.DOTALL)
    data = chop.sub(config.get('YOUR_DOMAIN_NAME'), data)

    chop = re.compile('STATISTICS_FE_OAUTH_CLIENT_ID_PLACEHOLDER', re.DOTALL)
    data = chop.sub(config.get('STATISTICS_FE_OAUTH_CLIENT_ID'), data)

    chop = re.compile('STATISTICS_BE_OAUTH_CLIENT_SECRET_PLACEHOLDER', re.DOTALL)
    data = chop.sub(config.get('STATISTICS_BE_OAUTH_CLIENT_SECRET'), data)

    chop = re.compile('STATISTICS_BE_OAUTH_CLIENT_ID_PLACEHOLDER', re.DOTALL)
    data = chop.sub(config.get('STATISTICS_BE_OAUTH_CLIENT_ID'), data)

    chop = re.compile('BPA_FE_OAUTH_CLIENT_ID_PLACEHOLDER', re.DOTALL)
    data = chop.sub(config.get('BPA_FE_OAUTH_CLIENT_ID'), data)

    chop = re.compile('BPA_BE_OAUTH_CLIENT_SECRET_PLACEHOLDER', re.DOTALL)
    data = chop.sub(config.get('BPA_BE_OAUTH_CLIENT_SECRET'), data)

    chop = re.compile('BPA_BE_OAUTH_CLIENT_ID_PLACEHOLDER', re.DOTALL)
    data = chop.sub(config.get('BPA_BE_OAUTH_CLIENT_ID'), data)

    chop = re.compile('CAMUNDA_OAUTH_CLIENT_SECRET_PLACEHOLDER', re.DOTALL)
    data = chop.sub(config.get('CAMUNDA_OAUTH_CLIENT_SECRET'), data)

    chop = re.compile('CAMUNDA_OAUTH_CLIENT_ID_PLACEHOLDER', re.DOTALL)
    data = chop.sub(config.get('CAMUNDA_OAUTH_CLIENT_ID'), data)

    chop = re.compile('DS_OAUTH_CLIENT_SECRET_PLACEHOLDER', re.DOTALL)
    data = chop.sub(config.get('DS_OAUTH_CLIENT_SECRET'), data)

    chop = re.compile('DS_OAUTH_CLIENT_ID_PLACEHOLDER', re.DOTALL)
    data = chop.sub(config.get('DS_OAUTH_CLIENT_ID'), data)

    chop = re.compile('GDB_OAUTH_CLIENT_SECRET_PLACEHOLDER', re.DOTALL)
    data = chop.sub(config.get('GDB_OAUTH_CLIENT_SECRET'), data)

    chop = re.compile('GDB_OAUTH_CLIENT_ID_PLACEHOLDER', re.DOTALL)
    data = chop.sub(config.get('GDB_OAUTH_CLIENT_ID'), data)

    chop = re.compile('MAIL_HOST_PLACEHOLDER', re.DOTALL)
    data = chop.sub(config.get('MAIL_HOST'), data)

    chop = re.compile('MAIL_REPLY_TO_PLACEHOLDER', re.DOTALL)
    data = chop.sub(config.get('MAIL_REPLY_TO'), data)

    chop = re.compile('MAIL_FROM_PLACEHOLDER', re.DOTALL)
    data = chop.sub(config.get('MAIL_FROM'), data)

    chop = re.compile('MAIL_PORT_PLACEHOLDER', re.DOTALL)
    data = chop.sub(config.get('MAIL_PORT'), data)

    chop = re.compile('MAIL_PASSWORD_PLACEHOLDER', re.DOTALL)
    data = chop.sub(config.get('MAIL_PASSWORD'), data)

    chop = re.compile('MAIL_USERNAME_PLACEHOLDER', re.DOTALL)
    data = chop.sub(config.get('MAIL_USERNAME'), data)

    with open(config.get('PATH_TO_SCRIPT_DIR') + 'keycloak-realm.json', "w") as f:
        f.write(data)

    subprocess.run('cp /opt/eregistrations/scripts/keycloak-realm.json /opt/eregistrations/compose/eregistrations/keycloak-realm.json', shell=True)

    print("./keycloak-realm.json file prepared to be imported")
    print("")


def auth_service_preparation():
    global auth_service
    print_title('auth_service_preparation')
    if not auth_service:
        auth_selection(True)
    if auth_service == "KEYCLOAK":
        return prepare_keycloak_realm()
    dont_return_true_before_cas_backend_is_running()
    for key, value in config.items():
        if key.upper().endswith('_OAUTH_CLIENT_ID'):
            create_micro_service(key.upper()[:-16])


def remove_unneeded_auth_sections(a_service, data):
    if a_service == "KEYCLOAK":
        chop = re.compile('( |\t)*### CAS ###\n(\n|.)*?### CAS end ###\n', re.MULTILINE)
        data = chop.sub('', data)

        chop = re.compile('( |\t)*### KEYCLOAK ###\n', re.DOTALL)
        data = chop.sub('', data)

        chop = re.compile('( |\t)*### KEYCLOAK end ###\n', re.DOTALL)
        data = chop.sub('', data)
    else:
        chop = re.compile('( |\t)*### KEYCLOAK ###\n(\n|.)*?### KEYCLOAK end ###\n', re.MULTILINE)
        data = chop.sub('', data)

        chop = re.compile('( |\t)*### CAS ###\n', re.DOTALL)
        data = chop.sub('', data)

        chop = re.compile('( |\t)*### CAS end ###\n', re.DOTALL)
        data = chop.sub('', data)
    return data


def translation_service_cleanup():
    print_title('translation service cleanup')
    chop = re.compile('(( |\t)*### TRANSLATIONS ###\n|( |\t)*### TRANSLATIONS end ###\n)', re.MULTILINE)
    if config.get('TRANSLATIONS_SYSTEM_INTERNAL') == 'False':
        chop = re.compile('( |\t)*### TRANSLATIONS ###\n(\n|.)*?### TRANSLATIONS end ###\n', re.MULTILINE)

    with open(config.get('PATH_TO_COMPOSE_CONF') + '.env', "r") as f:
        data = f.read()

    with open(config.get('PATH_TO_COMPOSE_CONF') + '.env', "w") as f:
        f.write(chop.sub('', data))
    print(".env translation service related cleanup done")
    print()
    return


def auth_service_choice_cleanup(selection):
    if selection == "C":
        selection = "CAS"
    if selection == "K":
        selection = "KEYCLOAK"

    if selection != "CAS" and selection != "KEYCLOAK":
        print('Provided authentication service value is not supported.')
        print('Supported authentication services are: CAS or KEYCLOAK')
        return False
    return selection


def auth_selection(skip_cleanup):
    global auth_service
    print_title('select authentication service')
    if not auth_service:
        result = input("Select authentication service that is used in your instance? [(S)kip/(C)as/(K)eycloak] ").upper()
        if result == "S" or result == "SKIP":
            print("Skipping...")
            return
        result = auth_service_choice_cleanup(result)
        if not result:
            return auth_selection(False)
        auth_service = result
    print("authentication service '%s' selected" % auth_service)
    print()

    if skip_cleanup:
        return

    with open(config.get('PATH_TO_HAPROXY_CONF') + 'haproxy.cfg', "r") as f:
        data = f.read()

    with open(config.get('PATH_TO_HAPROXY_CONF') + 'haproxy.cfg', "w") as f:
        f.write(remove_unneeded_auth_sections(auth_service, data))
    print("haproxy.cfg auth_service related cleanup done")

    with open(config.get('PATH_TO_COMPOSE_CONF') + 'docker-compose.yml', "r") as f:
        data = f.read()

    with open(config.get('PATH_TO_COMPOSE_CONF') + 'docker-compose.yml', "w") as f:
        f.write(remove_unneeded_auth_sections(auth_service, data))
    print("docker-compose.yml auth_service related cleanup done")

    with open(config.get('PATH_TO_COMPOSE_CONF') + '.env', "r") as f:
        data = f.read()

    with open(config.get('PATH_TO_COMPOSE_CONF') + '.env', "w") as f:
        f.write(remove_unneeded_auth_sections(auth_service, data))
    print(".env auth_service related cleanup done")
    print()
    return


def generate_pwds(force):
    print_title('(re)generate passwords')

    if not force:
        result = input("Are you sure you want to (re)generate all passwords which can be (re)generated? [Y/n] ").upper()
        if result == "N" or result == "NO":
            print("pwd (re)generation skipped...")
            return
    print("Will (re)generate pwds now")

    with open(env_path, "r+") as env:
        env_content = env.read()

    for item in auto_generatable_pwds:
        target = item
        pwd = subprocess.run("openssl rand -base64 20", shell=True, stderr=subprocess.PIPE,
                stdout=subprocess.PIPE).stdout.decode('utf-8').strip()
        pwd = pwd.replace('=', '').replace('+', '').replace('/', '')
        print(pwd)
        env_content = re.sub(rf"\n({target})\s*=\s*.+\n", rf"\n\1={pwd}\n", env_content)

    with open(env_path, "w") as env:
        env.write(env_content)

    config = dotenv_values(dotenv_path=env_path)
    print('passwords (re)generated successfully')


def run_task(first_time, task_name):
    force = False
    if task_name:
        force = True
    else:
        if first_time:
            print('Enter step name that you wish to run:')
        for step in supported_steps.items():
            print(f"\t{step[0]}: {step[1]}")
        task_name = input()

    if task_name == 'all':
        translation_service_cleanup()
        auth_selection(False)
        create_postgres_databases()
        create_mongo_databases()
        configure_haproxy()
        create_docker_network()
        auth_service_preparation()
        return deploy_docker_images()
    elif task_name == 'translation':
        return translation_service_cleanup()
    elif task_name == 'pwd':
        return generate_pwds(force)
    elif task_name == 'auth':
        return auth_selection(False)
    elif task_name == 'postgres':
        return create_postgres_databases()
    elif task_name == 'mongo':
        return create_mongo_databases()
    elif task_name == 'haproxy':
        return configure_haproxy()
    elif task_name == 'network':
        return create_docker_network()
    elif task_name == 'deploy':
        return deploy_docker_images()
    elif task_name == 'prepare_auth_service':
        return auth_service_preparation()
    else:
        print('Provided step name does not match to any of the supported steps')
        print('Supported step names are:')
        return run_task(False, False)


def main():
    global auth_service
    task_name = None
    parser.add_argument("--auth-service", choices=['CAS', 'KEYCLOAK'],
                        help="Authentication service that is used in your instance")
    for step in supported_steps.items():
        parser.add_argument(f"--step-{step[0].replace('_', '-')}", action='store_true', help=f"{step[1]}")
    args = parser.parse_args()
    if args.auth_service:
        cleaned_auth_service = auth_service_choice_cleanup(args.auth_service.upper())
        if cleaned_auth_service:
            auth_service = cleaned_auth_service
    for argument in vars(args).items():
        if argument[0].startswith('step_') and argument[1]:
            task_name = argument[0].replace('step_', '')
            break  # only one step can be run at a time
    run_task(True, task_name)


if __name__ == "__main__":
    main()
